package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@SpringBootApplication
@RestController
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	private ArrayList<String> enrollees = new ArrayList<String>();
	public DiscussionApplication() {
		enrollees = new ArrayList<String>();
	}

	@GetMapping("/enroll")
	public String enrollUser(@RequestParam("user") String username) {
		enrollees.add(username);
		return "User " + username + " enrolled successfully!";
	}

	@GetMapping("/getEnrollees")
	public String getEnrollees() {
		return enrollees.toString();
	}

	@GetMapping("/nameage")
	public String getNameAndAge(@RequestParam("name") String name, @RequestParam("age") int age) {
		return "Hello " + name + "! My age is " + age + ".";
	}

	@GetMapping("/courses/{id}")
	public String getCourseDetails(@PathVariable String id) {
		if (id.equals("java101")) {
			return "Java 101, MWF 8:00AM-11:00AM, PHP 3000.00";
		}
		if (id.equals("sql101")) {
			return "SQL 101, TTH 1:00PM-4:00PM, PHP 2000.00";
		}
		if (id.equals("javaee101")) {
			return "java EE 101, MWF 1:00PM-4:00PM, PHP 3500.00";
		}
		else {
			return "Course not found";
		}
	}
}
